/// <summary>
/// Main menu.
/// Attached to Main Camera
/// 
/// 9 px (scale 0.75 - ldpi)
/// 12 px (scale 1 - mdpi)
/// 18 px (scale 1.5 - hdpi)
/// 24 px (scale 2 - xhdpi)
/// </summary>
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {
	//	public static int screenDPI = Screen.dpi;
	//Sound handle
	public Button soundOnOff;
	public Sprite spriteSoundOff;
	public Sprite spriteSoundOn;
	bool sound = true;
	public AudioSource MainTheme;
	//Button resize
	public Button HighScore;
	public Button Store;
	public Button Game;
	// Swipe handle
	public float minSwipeDistY;
	public float minSwipeDistX;
	private Vector2 startPos;

	void Start(){
		/*switch (screenDPI) {
			case 96:
				//Use normal textures
			break;
			case screenDPI<96:
				//Use low textures
			break;
			case screenDPI>96:
				//Use high textures
			break;
		default:
				//Use low textures
			break;
		}*/
	}
	
	public void LoadScene(string sceneName){
		Application.LoadLevel(sceneName);
	}
	public void ChangeSoundSprite(){
		if (sound) {
			MainTheme.audio.Stop ();
			soundOnOff.image.sprite = spriteSoundOff;
		} else {
			MainTheme.audio.Play ();
			soundOnOff.image.sprite = spriteSoundOn;
		}
		sound = !sound;
	}
}