﻿using UnityEngine;
using System.Collections;

public class BloonCat : MonoBehaviour {
	public int catFlyingState;
	Animator bloonCatanimator;
	int flyHash;
	public AudioSource catSaysMeow;

	void Start(){
		bloonCatanimator = GetComponent<Animator> ();
		flyHash = Animator.StringToHash("catFlyTrigger");
	}
	void playMeow(){
		catSaysMeow.PlayDelayed (1);
	}
	public void letBloonCatFly(){
		bloonCatanimator.SetTrigger (flyHash);
		playMeow ();
	}
}
