﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Game : MonoBehaviour {
	public BloonCat bloonCatFly;
	//Sound controller
	public Button soundOnOff;
	public Sprite spriteSoundOff;
	public Sprite spriteSoundOn;
	bool sound = true;
	public AudioSource MainTheme;
	//BloonCat
	public Image bloonCat;
	//210 * 250
	public Button buttonTall;
	// Text to show count of clicks
	public Text clicksCount;
	public Text startMessage;
	//Big scope of upgrade buttons
	//Multiplier
	public Updates MultiplierX;
	//Autoclicker
	private bool dontStartAutoClicks;
	public Updates AutoclickerX;


	void Start(){
		ClicksManager.GlobalClicks = 0;
		dontStartAutoClicks = false;
		startMessage.text = "Now, tap the tall!";
		clicksCount.text = "";
		//Create and show updates on start
		AutoclickerX = new Updates (180, "Autoclicks x");
		MultiplierX = new Updates (10, "Increase ");
		showAvailableUpdates ();
	}
	//
	// Update
	//
	//
	void Update(){
		if (!dontStartAutoClicks) {
			if (AutoclickerX.getValue() != 0) {
				InvokeRepeating ("AutoClickersAction", 0, 1);
				dontStartAutoClicks = true;
			}
		}
	}
	//
	// /Update
	//
	//
	//
	// Upgrade buttons
	//
	//
	void showAvailableUpdates(){
		//TODO
		AutoclickerX.SetButtonState ();
		MultiplierX.SetButtonState ();
	}
	//AutoClicker
	public void BuyAutoClicker (){
		AutoclickerX.Buy ();
		SetClicksCount ();
	}
	private void AutoClickersAction (){
		ClicksManager.GlobalClicks += AutoclickerX.getValue();
		SetClicksCount ();
	}
	//Multiplier
	public void BuyMultiplier (){
		MultiplierX.Buy ();
		SetClicksCount ();
	}
	//
	//
	// /Upgrade buttons
	//
	public void ClickTall(){
		if (GameObject.Find("Started text")) {
			Destroy (startMessage);
		}
		ClicksManager.GlobalClicks += 1*MultiplierX.getValue();
		SetClicksCount ();
	}
	void SetClicksCount(){
		clicksCount.text = "Balloons: " + ClicksManager.GlobalClicks.ToString();
		if((ClicksManager.GlobalClicks != 0) && (ClicksManager.GlobalClicks % 135 == 0)){
			bloonCatFly.letBloonCatFly ();
		}
		showAvailableUpdates ();
	}
	public void LoadScene(string sceneName){
		Application.LoadLevel(sceneName);
	}
	public void ChangeSoundSprite(){
		if (sound) {
			MainTheme.audio.Stop ();
			soundOnOff.image.sprite = spriteSoundOff;
		} else {
			MainTheme.audio.Play ();
			soundOnOff.image.sprite = spriteSoundOn;
		}
		sound = !sound;
	}
}